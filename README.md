# Instructions for the installation of the 2HDMC-HB5-HS2 interface

1. Compile the program xsecHptb.F90, which obtains the 13 TeV cross section for pp->H+^t + X from the grids of the LHC HXSWG:

       gfortran xsecHptb.F90 -o xsecHptb

2. Run the program xsecHptb once on an example point in order to create a binary file that contains the relevant data:

	    ./xsecHptb 2 500 10

3. Copy both the executable `xsecHptb` and the binary file `xsecHptb13TeV.binary` to the working directory of the scans. This path is assumed when the 2HDMC HBHS-interface is executed, see `HBHS.cpp`, starting at line ~ 420. (*There is probably a better way to do this!*)

4. Patch the 2HDMC calculator with the provided files in the `src/` directory.

5. Install HiggsBounds and HiggsSignals programs and copy their libraries into the 2HDMC `lib/` directory.

Have fun!
