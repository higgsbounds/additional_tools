#include "THDM.h"
#include "HBHS.h"
#include "Constraints.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[]) {

  if (argc < 1) {
    cout << "Usage: ./ScanInert output_filename\n";
    return -1;
  }
  
  
// Give parameters some default values (possibly changed later in the scan, see below.)
  double mh_in = 125.0;   //   SM-like (non-inert) Higgs
  double mH_in = 65.0;    //   inert neutral Higgs boson
  double mA_in = 500.0;   //   inert neutral Higgs boson
  double mHp_in = 500.0;  //   inert charged Higgs boson
  double l2_in = 0.5;     //   (n.b.:   only relevant for tree-couplings in inert sector )
  double lL_in = 0.005;   //   (n.b.:   H H h coupling ~ lambdaL )
  
// For a parameter point identical to the SM prediction, set lambda_3 = 0,
// and make mH > mh/2.
//----------------------

  double l3_in;
  char* file = argv[1];  
    
  THDM model;
  SM sm;  

  double v2 = sm.get_v2();
  
#if defined HiggsBounds
  HB_init();  
  HS_init();
  HS_set_output_level(0);
//   HS_setup_assignment_range_massobservables(2.);
  
#endif


  std::ofstream outfile(file);

// Perform a 1-D parameter scan 
  for (int i=20; i<=20; i++){

//-- (1) Scan over the light inert Higgs boson mass:  

//   for (int i=1; i<=141; i++){  
  mH_in = 0.5 + (i-1)*0.5;

//-- Derive lambda_3 in terms of lambda_L and inert Higgs masses.

  l3_in = 2./v2*(pow(mHp_in,2.) - pow(mH_in,2.)) + 2.*lL_in;  

//-- (2) Scan over the inert charged Higgs boson mass:  

//   for (int i=1; i<=201; i++){  

//   mHp_in = 100. + (i-1)*2.;

//-- Set lambda_3 directly:   (n.b.:   H+H-h coupling ~ lambda3 )

//   l3_in = 0.5;
//   lL_in = l3_in/2. - 1./v2*(pow(mHp_in,2.) - pow(mH_in,2.));

  
//-- Setup 2HDM parameter point  
  bool pset = model.set_inert(mh_in,mH_in,mA_in,mHp_in,l2_in,l3_in);
  
  if (!pset) {
    cerr << "The parameters you have specified were not valid\n";
    return -1;
  }
  
//-- Extract BR(h-> invisible) here:
  double BRhinvisible;
  DecayTable table(model);
  
  if(mH_in < mA_in){
   BRhinvisible = table.get_gamma_hhh(2,1,1)/table.get_gammatot_h(2);
   }
  else if(mH_in > mA_in){
   BRhinvisible = table.get_gamma_hhh(2,3,3)/table.get_gammatot_h(2);  
   }
  else {
   BRhinvisible = ( table.get_gamma_hhh(2,1,1)+table.get_gamma_hhh(2,3,3) )/table.get_gammatot_h(2);  
   }
//   printf("(2HDMC internal) BR(h->invisible) = %16.8E\n",BRhinvisible);

//-- Check theoretical and EW precision constraints:

// Reference SM Higgs mass for EW precision observables
  double mh_ref = 125.;
  Constraints check(model);  
  check.print_all(mh_ref);

//-- HiggsBounds and HiggsSignals
#if defined HiggsBounds
//-- Calculate and pass input to HiggsBounds (see HBHS.cpp for details)
  HB_set_input_effC(model);

//-- Create arrays to hold the HiggsBounds results (0th entry is global result, 1-4th entry for each Higgs)
  int hbres[5];
  double hbobs[5];
  int hbchan[5];
  int hbcomb[5];  

//-- Run HiggsBounds 'full', i.e. with each Higgs result separately  
  HB_run_full(hbres, hbchan, hbobs, hbcomb);
  printf("\nHiggsBounds results (full):\n");
  printf("  Higgs  res  chan       ratio        ncomb\n");
  for (int i=1;i<=4;i++) {
    printf("%5d %5d %6d %16.8E %5d   %s\n", i, hbres[i],hbchan[i],hbobs[i],hbcomb[i],hbobs[i]<1 ? "Allowed" : "Excluded");
  }
  printf("------------------------------------------------------------\n");
  printf("  TOT %5d %6d %16.8E %5d   %s\n", hbres[0],hbchan[0],hbobs[0],hbcomb[0],hbobs[0]<1 ? "ALLOWED" : "EXCLUDED");
  
//-- Get more information from HiggsBounds: We want to know the first three most sensitive searches are for the SM-like Higgs boson.  
  int index, hbreshSM[3], chan[3], ncombined[3];
  double  obsratio[3], predratio[3];
  index = 2;
  for (int rank=1; rank <= 3; rank++){
  HB_get_most_sensitive_channels_per_Higgs(index, rank, hbreshSM[rank-1], chan[rank-1], obsratio[rank-1], predratio[rank-1], ncombined[rank-1]);

  printf("%2dth sensitive channel for SM-like Higgs: %5d %5d %16.8E %16.8E\n",rank, hbreshSM[rank-1], chan[rank-1], obsratio[rank-1], predratio[rank-1]);
  }

  double csqmu;
  double csqmh;
  double csqtot;
  int nobs;
  double pval;
   
  HS_run(&csqmu, &csqmh, &csqtot, &nobs, &pval);

  printf("\nHiggsSignals results:\n");
  printf(" Chi^2 from rates: %16.8E\n", csqmu);
  printf("  Chi^2 from mass: %16.8E\n", csqmh);
  printf("      Total chi^2: %16.8E\n", csqtot);
  printf("    # observables: %16d\n\n", nobs);
 
  double R_H_WW, R_H_ZZ, R_H_gaga, R_H_tautau, R_H_bb, R_VH_bb;
 
  HS_get_Rvalues(2, 4, R_H_WW, R_H_ZZ, R_H_gaga, R_H_tautau, R_H_bb, R_VH_bb);
  
  outfile << mh_in << " " << mH_in << " " << mA_in << " " << mHp_in << " " 
          << l2_in << " " << l3_in <<" " << lL_in << " " << BRhinvisible << " "
          << R_H_WW <<" " << R_H_gaga << " " 
          << hbreshSM[0] << " " << chan[0] << " " << obsratio[0] << " "<< predratio[0] << " "
          << hbreshSM[1] << " " << chan[1] << " " << obsratio[1] << " "<< predratio[1] << " "
          << hbreshSM[2] << " " << chan[2] << " " << obsratio[2] << " "<< predratio[2] << " "
          << csqmu << endl;
  
#endif

//   model.print_param_phys();
//   model.print_param_gen();
//   model.print_param_higgs();
//   model.write_LesHouches(file,true,true,true,false);
 
  }
}
