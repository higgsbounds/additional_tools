#include "HBHS.h"
#include "THDM.h"
#include "DecayTable.h"
#include "SM.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>


static bool HB_initialized = false;
static bool HS_initialized = false;

#if defined HiggsBounds


void HB_init() {
  int nH0=3;
  int nHp=1;
  int hbflag=3;

  if (HB_initialized) return;

  printf("\nInitializing HiggsBounds... ");

   // Third argument is HB analysis setting: 1='onlyL', 2='onlyH' 3='LandH'
    initialize_higgsbounds_int_(&nH0, &nHp, &hbflag);
//  initialize_higgsbounds_(&nH0,&nHp,whichexpt);

  printf("Please cite the relevant publications when using Higgs mass limits.\n");

  HB_initialized = true;

}


void HB_set_input(THDM model) {

	HB_set_input_effC(model);

}


void HB_set_input_effC(THDM model) {

	if (!HB_initialized) {
		cout << "WARNING: HiggsBounds must be initialized with HB_init() before usage" << endl;
		return;
	}

    bool debug = false;

    double Mh[3];
    double GammaTotal[3];
//     double GammaTotalSM[3];
    double ghjss_s[3];
    double ghjss_p[3];
    double ghjcc_s[3];
    double ghjcc_p[3];
    double ghjbb_s[3];
    double ghjbb_p[3];
    double ghjtt_s[3];
    double ghjtt_p[3];
    double ghjmumu_s[3];
    double ghjmumu_p[3];
    double ghjtautau_s[3];
    double ghjtautau_p[3];
    double ghjWW[3];
    double ghjZZ[3];
    double ghjZga[3];
    double ghjgaga[3];
    double ghjgg[3];
//     double ghjggZ[3];
    double ghjhiZ[3][3];

// non-SM decay branching ratios
    double BR_hjinvisible[3];
    double BR_hkhjhi[3][3][3];
    double BR_hjhiZ[3][3];
	double BR_hjHpW[1][3];
	double BR_hjemu[3];
	double BR_hjetau[3];
	double BR_hjmutau[3];

// SM decay branching ratios
// 	double BR_hjss[3];
// 	double BR_hjcc[3];
// 	double BR_hjbb[3];
// 	double BR_hjtt[3];
// 	double BR_hjmumu[3];
// 	double BR_hjtautau[3];
// 	double BR_hjWW[3];
// 	double BR_hjZZ[3];
// 	double BR_hjZga[3];
// 	double BR_hjgaga[3];
// 	double BR_hjgg[3];

    double MHp[1];
	double MHplusGammaTot[1];
	double CS_lep_HpjHmi_ratio[1];
	double BR_tWpb[1];
	double BR_tHpjb[1];
	double BR_Hpjcs[1];
	double BR_Hpjcb[1];
	double BR_Hptaunu[1];
	double BR_Hptb[1];
	double BR_HpWZ[1];
	double BR_HphiW[3][1];
	double CS_Hpjtb[1];
	double CS_Hpjbjet[1];
	double CS_Hpjcb[1];
	double CS_Hpjcjet[1];
	double CS_Hpjjetjet[1];
	double CS_HpjW[1];
	double CS_HpjZ[1];
	double CS_vbf_Hpj[1];
	double CS_HpjHmj[1];
	double CS_Hpjhi[3][1];
    int CP_value[3];
    double Gamma_htohh;
    double Gamma_htoZh;
    double Gamma_htoHpW;
    double Gamma_htotSM;

    int type;
    char str[100];

    double sba, tanb, lam6, lam7, m122;
    model.get_param_phys(Mh[0], Mh[1], Mh[2], MHp[0], sba, lam6, lam7, m122, tanb);
	type = model.get_yukawas_type();

#if defined debug
    printf("Masses for HB/HS: %16.8E %16.8E %16.8E\n", Mh[0], Mh[1], Mh[2]);
#endif

    THDM sm_like;
    DecayTable table(model), sm_table(sm_like);

    SM sm = model.get_SM();

  	double g=sm.get_g();
  	double costw=sm.get_costw();
//   	double mt = sm.get_umass_pole(3);

    complex <double> c,cs,cp,c_sm,cs_sm,cp_sm;

      CP_value[0] = +1;
      CP_value[1] = +1;
      CP_value[2] = -1;

    for (int h=1;h<=3;h++) {
//       double mh = Mh[h-1];
//      sm_like.set_param_phys(Mh[h-1], Mh[h-1]*10., Mh[h-1]*10., Mh[h-1]*10., 1.0, 0., 0., 0., 1.0);
      sm_like.set_param_sm(Mh[h-1]);
      sm_like.set_yukawas_type(1);

      table.set_model(model);
      sm_table.set_model(sm_like);

//  NEUTRAL HIGGS INPUT
// n.b.: In general the g's should also contain the sign of the SM normalized coupling!
      const std::complex<double> i(0, 1);
      model.get_coupling_hdd(h,2,2,cs,cp);
	    sm_like.get_coupling_hdd(1,2,2,cs_sm,cp_sm);
      ghjss_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjss_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));

	  if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "ss", ghjss_s[h-1], ghjss_p[h-1]);

      model.get_coupling_hdd(h,3,3,cs,cp);
	  sm_like.get_coupling_hdd(1,3,3,cs_sm,cp_sm);
      ghjbb_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjbb_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));

      if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "bb", ghjbb_s[h-1], ghjbb_p[h-1]);

      model.get_coupling_huu(h,2,2,cs,cp);
	  sm_like.get_coupling_huu(1,2,2,cs_sm,cp_sm);
      ghjcc_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjcc_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));
      if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "cc", ghjcc_s[h-1], ghjcc_p[h-1]);

      model.get_coupling_huu(h,3,3,cs,cp);
	  sm_like.get_coupling_huu(1,3,3,cs_sm,cp_sm);
      ghjtt_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjtt_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));
      if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "tt", ghjtt_s[h-1], ghjtt_p[h-1]);

      model.get_coupling_hll(h,2,2,cs,cp);
	    sm_like.get_coupling_hll(1,2,2,cs_sm,cp_sm);
      ghjmumu_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjmumu_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));
      if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "mumu", ghjmumu_s[h-1], ghjmumu_p[h-1]);

      model.get_coupling_hll(h,3,3,cs,cp);
	    sm_like.get_coupling_hll(1,3,3,cs_sm,cp_sm);
      ghjtautau_s[h-1] = abs(cs/cs_sm)*real(exp(i*arg(cs/cs_sm)));
      ghjtautau_p[h-1] = abs(cp/cs_sm)*imag(exp(i*arg(cp/cs_sm)));
      if (debug) printf("%2d %5s %16.8E %16.8E\n", h, "tata", ghjtautau_s[h-1], ghjtautau_p[h-1]);

      model.get_coupling_vvh(2, 2, h, c);
      sm_like.get_coupling_vvh(2, 2, 1, c_sm);
      ghjZZ[h-1] = abs(c/c_sm)*real(exp(i*arg(c/c_sm)));
      if (debug) printf("%2d %5s %16.8E\n", h, "ZZ", ghjZZ[h-1]);

      model.get_coupling_vvh(3, 3, h, c);
      sm_like.get_coupling_vvh(3, 3, 1, c_sm);
      ghjWW[h-1] = abs(c/c_sm)*real(exp(i*arg(c/c_sm)));
      if (debug) printf("%2d %5s %16.8E\n", h, "WW", ghjWW[h-1]);

      double hgaga = table.get_gamma_hgaga(h);
      double hgaga_sm = sm_table.get_gamma_hgaga(1);
      ghjgaga[h-1] = sqrt(hgaga/hgaga_sm);
      if (debug) printf("%2d %5s %16.8E\n", h, "gaga",ghjgaga[h-1]);

      double hZga = table.get_gamma_hZga(h);
      double hZga_sm = sm_table.get_gamma_hZga(1);
      ghjZga[h-1] = sqrt(hZga/hZga_sm);
      if (debug) printf("%2d %5s %16.8E\n", h, "Zga", ghjZga[h-1]);

      double hgg = table.get_gamma_hgg(h);
      double hgg_sm = sm_table.get_gamma_hgg(1);
      ghjgg[h-1] = sqrt(hgg/hgg_sm);
      if (debug) printf("%2d %5s %16.8E\n", h, "gg", ghjgg[h-1]);

//  Set branching ratios directly:
//       BR_hjss[h-1] = table.get_gamma_hdd(h,2,2)/table.get_gammatot_h(h);
//       BR_hjcc[h-1] = table.get_gamma_huu(h,2,2)/table.get_gammatot_h(h);
//       BR_hjbb[h-1] = table.get_gamma_hdd(h,3,3)/table.get_gammatot_h(h);
//       BR_hjtt[h-1] = table.get_gamma_huu(h,3,3)/table.get_gammatot_h(h);
//       BR_hjmumu[h-1] = table.get_gamma_hll(h,2,2)/table.get_gammatot_h(h);
//       BR_hjtautau[h-1] = table.get_gamma_hll(h,3,3)/table.get_gammatot_h(h);
//       BR_hjWW[h-1] = table.get_gamma_hvv(h,3)/table.get_gammatot_h(h);
//       BR_hjZZ[h-1] = table.get_gamma_hvv(h,2)/table.get_gammatot_h(h);
//       BR_hjZga[h-1] = table.get_gamma_hZga(h)/table.get_gammatot_h(h);
//       BR_hjgaga[h-1] = table.get_gamma_hgaga(h)/table.get_gammatot_h(h);
//       BR_hjgg[h-1] = table.get_gamma_hgg(h)/table.get_gammatot_h(h);
//
//       BR_hjemu[h-1]=table.get_gamma_hll(h,1,2)/table.get_gammatot_h(h);
//       BR_hjetau[h-1]=table.get_gamma_hll(h,1,3)/table.get_gammatot_h(h);
//       BR_hjmutau[h-1]=table.get_gamma_hll(h,2,3)/table.get_gammatot_h(h);
//       BR_hjHpW[1][h-1]=table.get_gamma_hvh(h,3,4)/table.get_gammatot_h(h);
      BR_hjinvisible[h-1]=0.0;

      Gamma_htohh = 0.0;
      Gamma_htoZh = 0.0;
      Gamma_htoHpW = 0.0;
      for (int j=1;j<=3;j++) {
       for (int k=1;k<j+1;k++) {
        Gamma_htohh = Gamma_htohh + table.get_gamma_hhh(h,j,k);
       }
       Gamma_htoZh = Gamma_htoZh + table.get_gamma_hvh(h,2,j);
      }
      Gamma_htoHpW = table.get_gamma_hvh(h,3,4);
      //NEW//
      Gamma_htohh = Gamma_htohh + table.get_gamma_hhh(h,4,4);

      if (debug) printf("ghtohh, ghtoZh, ghtoHpW %16.8E %16.8E %16.8E\n",  Gamma_htohh, Gamma_htoZh, Gamma_htoHpW);

      if (debug) printf("ghtoemu, ghtoetau, ghtomutau %16.8E %16.8E %16.8E\n", table.get_gamma_hll(h,1,2), table.get_gamma_hll(h,1,3), table.get_gamma_hll(h,2,3));
// Calculate the total width from HiggsBounds internal BR-functions and the effective couplings,
// and add the Higgs decays to non-SM final states.
      GammaTotal[h-1] = HB_get_gammah(Mh[h-1],  ghjss_s[h-1],  ghjss_p[h-1], ghjcc_s[h-1],
                    ghjcc_p[h-1], ghjbb_s[h-1],  ghjbb_p[h-1], ghjtt_s[h-1], ghjtt_p[h-1],
                    ghjmumu_s[h-1], ghjmumu_p[h-1], ghjtautau_s[h-1],  ghjtautau_p[h-1],
                    ghjWW[h-1], ghjZZ[h-1], ghjZga[h-1], ghjgaga[h-1], ghjgg[h-1])
                    + Gamma_htohh + Gamma_htoZh + Gamma_htoHpW
		     		//NEW//
		    		+ table.get_gamma_hll(h,1,2) + table.get_gamma_hll(h,1,3) + table.get_gamma_hll(h,2,3);

// 	  GammaTotal[h-1] = table.get_gammatot_h(h);

	// NEW, change 1 to 0 //
      BR_hjHpW[0][h-1]=table.get_gamma_hvh(h,3,4)/GammaTotal[h-1];
      //BR_hjHpW[1][h-1]=table.get_gamma_hvh(h,3,4)/GammaTotal[h-1];
      BR_hjemu[h-1]=table.get_gamma_hll(h,1,2)/GammaTotal[h-1];
      BR_hjetau[h-1]=table.get_gamma_hll(h,1,3)/GammaTotal[h-1];
      BR_hjmutau[h-1]=table.get_gamma_hll(h,2,3)/GammaTotal[h-1];

      if (debug) printf("gtot %16.8E %16.8E %16.8E %16.8E\n",  GammaTotal[h-1], table.get_gammatot_h(h), HB_get_gammahsm(Mh[h-1]), sm_table.get_gammatot_h(1));
    }

  	 for (int j=1;j<=3;j++) {
      for (int i=1;i<=3;i++) {
       for (int k=1;k<=3;k++) {
       double x = table.get_gamma_hhh(k,j,i)/GammaTotal[k-1];
        BR_hkhjhi[i-1][j-1][k-1]=(x == x ? x : 0.);
        }
       double x = table.get_gamma_hvh(j,2,i)/GammaTotal[j-1];
       BR_hjhiZ[i-1][j-1]=(x == x ? x : 0.);
       model.get_coupling_vhh(2,j,i,c);
       ghjhiZ[i-1][j-1]=real(c)/(g/2./costw);

       if (debug) printf("%2d %2d hj->hihi %16.8E\n", j, i, table.get_gamma_hhh(j,i,i)/GammaTotal[j-1]);
      }
    }

     if(type == 0){
     if(Mh[0] < Mh[2]){
      BR_hjinvisible[1] = BR_hkhjhi[0][0][1];
      }
     else if(Mh[2] < Mh[0]){
      BR_hjinvisible[1] = BR_hkhjhi[2][2][1];
     }
     else{
      BR_hjinvisible[1] = BR_hkhjhi[0][0][1] + BR_hkhjhi[2][2][1];
     }
	printf("BR_hSM->invisible = %16.8E\n",BR_hjinvisible[1]);

     }
	// NEW BY FRAN to print input //
// 	printf("*************** NEUTRAL BOSON IN 2HDMC ********************");
// 	printf("\nCP values %d %d %d \n", CP_value[0], CP_value[1], CP_value[2]);
// 	printf("Gamma total %E %E %E \n",GammaTotal[0], GammaTotal[1], GammaTotal[2]);

     for (int k=1;k<=3;k++) {
         for (int i=1;i<=3;i++) {
             model.get_coupling_vhh(2,k,i,c);
              ghjhiZ[i-1][k-1]=real(c)/(g/2./costw);
//              printf("ghjhiZ %d %d %E \n", i, k, ghjhiZ[i-1][k-1]);
         }
     }
//     for (int k=1;k<=3;k++) {
//         for (int j=1;j<=3;j++) {
//             for (int i=1;i<=3;i++) {
//                 printf("h->hh, %d %d %d %E \n", k, j, i, BR_hkhjhi[i-1][j-1][k-1]);
//             }
//         }
//     }
//     for (int k=1;k<=3;k++) {
//         for (int i=1;i<=3;i++) {
//             printf("hj->hiZ, %d %d %E \n", k, i, BR_hjhiZ[i-1][k-1]);
//         }
//     }
//     for (int k=1;k<=3;k++) {
//             printf("hj->hpW, %d %E \n", k, BR_hjHpW[0][k-1]);
//     }
//     for (int k=1;k<=3;k++) {
//             printf("hj->emu %d %E \n", k, BR_hjemu[k-1]);
//             printf("hj->etau %d %E \n", k, BR_hjetau[k-1]);
//             printf("hj->mutau %d %E \n", k, BR_hjmutau[k-1]);
//     }

    higgsbounds_neutral_input_properties_(Mh,GammaTotal,CP_value);

    higgsbounds_neutral_input_effc_(ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,
    ghjbb_s,ghjbb_p,ghjtt_s,ghjtt_p,
    ghjmumu_s,ghjmumu_p,
    ghjtautau_s,ghjtautau_p,
    ghjWW,ghjZZ,ghjZga,
    ghjgaga,ghjgg,ghjhiZ);

// n.b.: BRs are approximated within HiggsBounds using the effective couplings.

//     higgsbounds_neutral_input_smbr_(BR_hjss,BR_hjcc,BR_hjbb,BR_hjtt,
//      BR_hjmumu,BR_hjtautau,BR_hjWW,BR_hjZZ,BR_hjZga,BR_hjgaga,BR_hjgg);

    higgsbounds_neutral_input_nonsmbr_(BR_hjinvisible,
     BR_hkhjhi,BR_hjhiZ,BR_hjemu,BR_hjetau,BR_hjmutau,BR_hjHpW);

//  CHARGED HIGGS INPUT

  CS_lep_HpjHmi_ratio[0] = 1.;
	BR_tWpb[0] = sm.get_gamma_top()/table.get_gammatot_top();
	BR_tHpjb[0]=table.get_gamma_uhd(3,4,3)/table.get_gammatot_top();
	BR_Hpjcs[0] = table.get_gamma_hdu(4,2,2)/table.get_gammatot_h(4);
	BR_Hpjcb[0] = table.get_gamma_hdu(4,3,2)/table.get_gammatot_h(4);
	BR_Hptaunu[0] = table.get_gamma_hln(4,3,3)/table.get_gammatot_h(4);
	BR_Hptb[0] = table.get_gamma_hdu(4,3,3)/table.get_gammatot_h(4);
	BR_HpWZ[0] = 0.0;
    for (int i=1;i<=3;i++){
     BR_HphiW[i-1][0]= table.get_gamma_hvh(4,3,i)/table.get_gammatot_h(4);
    }

	// NEW BY FRAN to print output //
// 	printf("*************** CHARGED BOSON BR OF 2HDMC ********************");
// 	printf("\nt->W+b %E \nt->h+b %E \nh+->cs %E \nh+->cb %E \nh+->tau nu %E \nh+->tb %E \nh+->W+Z %E \n",BR_tWpb[0],BR_tHpjb[0],BR_Hpjcs[0],BR_Hpjcb[0],BR_Hptaunu[0],BR_Hptb[0],BR_HpWZ[0]);
// 	for (int i=1;i<=3;i++){
// 	printf("h+->hiW+ %d %E \n", i, BR_HphiW[i-1][0]);
// 	}

    higgsbounds_charged_input_(MHp,MHplusGammaTot,
    CS_lep_HpjHmi_ratio,BR_tWpb,BR_tHpjb,
    BR_Hpjcs,BR_Hpjcb,BR_Hptaunu,BR_Hptb,
    BR_HpWZ,BR_HphiW);

// IF CALCULATED, WE CAN SET THE 13 TEV LHC CROSS SECTIONS FOR DIRECT
// CHARGED HIGGS PRODUCTION HERE:
	CS_Hpjtb[0]=0;
	CS_Hpjcb[0]=0;
	CS_Hpjbjet[0]=0;
	CS_Hpjcjet[0]=0;
	CS_Hpjjetjet[0]=0;
	CS_HpjW[0]=0;
	CS_HpjZ[0]=0;
	CS_vbf_Hpj[0]=0;
	CS_HpjHmj[0]=0;
	CS_Hpjhi[0][0]=0;
	CS_Hpjhi[1][0]=0;
	CS_Hpjhi[2][0]=0;

//--
// THE FOLLOWING IS AN INTERFACE TO OBTAIN THE pp->Hp tb CROSS SECTION
// FROM AN EXTERNAL CODE (written by T.Stefaniak) THAT INTERPOLATES THE
// PREDICTIONS FROM THE LHC HIGGS CROSS SECTION WORKING GROUP,
// SEE https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGMSSMCharged
// FOR DETAILS.

// Do not set charged Higgs input in inert doublet model:
	if(type != 0){
 	if(type == -1){
// This is the general 2HDM model.
	double mu = 173.2;
	double ku, kc, kt, kd, ks, kb;
	double ru, rc, rt, rd, rs, rb;

	model.get_kappa_up(mu, ku, kc, kt);
	model.get_kappa_down(mu, kd, ks, kb);
	model.get_rho_down(mu, rd, rs, rb);
	model.get_rho_up(mu, ru, rc, rt);

//	Check if model is sufficiently Type 2:
	if(abs(abs(rt/kt*rb/kb)-1.0) <= (5.0E-2*max(rt/kt, rb/kb))) {
	 	sprintf(str,"./xsecHptb %1d %10.2f %10.2f > xsec13Hptb.out",2,MHp[0],abs(1./(rt/kt)));
	}
//	Check if model is sufficiently Type 1:
	else if(abs(abs(rt/kt)-abs(rb/kb)) <= (5.0E-2*max(rt/kt, rb/kb))) {
		sprintf(str,"./xsecHptb %1d %10.2f %10.2f > xsec13Hptb.out",1,MHp[0],abs(1./(rt/kt)));
	}
	else{
		sprintf(str,"./xsecHptb %1d %10.2f %10.4f %10.4f > xsec13Hptb.out",type,MHp[0],rt/kt,rb/kb);
	}
  	}
  	else{
//  This is a 2HDM with a discrete Z2 symmetry.
	sprintf(str,"./xsecHptb %1d %10.2f %10.2f > xsec13Hptb.out",type,MHp[0],tanb);
	}
	system(str);

	string line;
	ifstream myfile ("xsec13Hptb.out");
	if (myfile.is_open()) {
	 getline (myfile,line);
	CS_Hpjtb[0]=2.0*stof(line);
    }
	myfile.close();
//--END OF INTERFACE

	int collider = 13;

	// NEW BY FRAN to print output //
// 	printf("*************** CHARGED BOSON XSECTIONS OF 2HDMC ********************");
// 	printf("\ncollider %d \nxs h+tb %E \nxs h+cb %E \nxs h+bjet %E \nxs h+cjet %E \nxs h+jetjet %E \nxs h+W- %E \nxs h+Z %E \nxs VBF h+ %E \nxs h+h- %E \n",collider, CS_Hpjtb[0], CS_Hpjcb[0], CS_Hpjbjet[0], CS_Hpjcjet[0], CS_Hpjjetjet[0], CS_HpjW[0], CS_HpjZ[0], CS_vbf_Hpj[0], CS_HpjHmj[0]);
// 	for(int i=1;i<=3;i++){
// 	printf("xs h+hj %d %E\n", i, CS_Hpjhi[i-1][0]);
// 	}
    higgsbounds_charged_input_hadr_(&collider, CS_Hpjtb, CS_Hpjcb, CS_Hpjbjet,
    CS_Hpjcjet, CS_Hpjjetjet, CS_HpjW, CS_HpjZ, CS_vbf_Hpj, CS_HpjHmj, CS_Hpjhi);

	}
}


double HB_get_gammahsm(double m) {

   return smgamma_h_(&m);
}

double HB_get_gammah(double m, double hss, double ass, double hcc, double acc,double hbb,double abb, double htt,double att,double hmumu,double amumu,double htautau,double atautau,double hww,double hzz,double hzga,double hgaga,double hgg) {
   return HB_get_gammahsm(m) * ( pow(hww,2.) * smbr_hww_(&m) +
                                 pow(hzz,2.) * smbr_hzz_(&m) +
                                 pow(hgg,2.) * smbr_hgg_(&m) +
//                                  pow(hzga,2.) * smbr_hzgam_(&m) +
                                 pow(hgaga,2.) * smbr_hgamgam_(&m) +
                                (pow(htt,2.) + pow(att,2.)*invbsq(173.2,m)) * smbr_htoptop_(&m) +
                                (pow(hbb,2.) + pow(abb,2.)*invbsq(4.2,m)) * smbr_hbb_(&m) +
                                (pow(htautau,2.) + pow(atautau,2.)*invbsq(1.777,m)) * smbr_htautau_(&m) +
                                (pow(hss,2.) + pow(ass,2.)*invbsq(0.105,m)) * smbr_hss_(&m) +
                                (pow(hcc,2.) + pow(acc,2.)*invbsq(1.27,m)) * smbr_hcc_(&m) +
                                (pow(hmumu,2.) + pow(amumu,2.)*invbsq(0.1057,m))*smbr_hmumu_(&m) );
}

double invbsq(double mf, double mh){
    if(mh > 2.0*mf){
            return 1.0/(1.0 - 4.0*pow((mf/mh),2.));
    }
    else{
            return 0.0;
    };
   }

void HB_run_full(int hbres[], int hbchan[], double hbobs[], int hbcomb[]) {

//    printf("Running HB full\n");
	if (!HB_initialized) {
		cout << "WARNING: HiggsBounds must be initialized with HB_init() before usage" << endl;
		return;
	}
	run_higgsbounds_full_(hbres,hbchan,hbobs,hbcomb);
}

void HB_get_most_sensitive_channels_per_Higgs(int nH,int pos,int &HBresult, int &chan, double &obsratio, double &predratio,int &ncombined) {

	higgsbounds_get_most_sensitive_channels_per_higgs_(&nH, &pos, &HBresult, &chan, &obsratio, &predratio, &ncombined);
}

void HS_init() {

 int nH0 = 3;
 int nHp = 1;
//  double range = 3.;

 printf("\nInitializing HiggsSignals... ");

 initialize_higgssignals_latestresults_(&nH0,&nHp);
//  initialize_higgssignals_lhc13_(&nH0,&nHp);

 HS_initialized = true;
}

void HS_finish() {

  finish_higgssignals_();
}

void HB_finish() {

  finish_higgsbounds_();
}

// void HB_deactivate_analyses(int *analysesID){
// 	__channels_MOD_higgsbounds_deactivate_analyses(analysesID);
// }

void HS_run(double *csqmu, double *csqmh, double *csqtot, int *nobs, double *pval) {
	if (!HS_initialized) {
		cout << "WARNING: HiggsSignals must be initialized with HS_init() before usage" << endl;
		return;
	}

	int mode = 1;
	int nobs1;
	int nobs2;
	int nobs3;
	double csqmu1;
	double csqmu2;
	double csqmu3;
	double csqmh1;
	double csqmh2;
	double csqmh3;
	double csqtot1;
	double csqtot2;
	double csqtot3;

//   run_higgssignals_(&mode, csqmu, csqmh, csqtot, nobs, pval);
  run_higgssignals_(&mode, &csqmu1, &csqmh1, &csqtot1, &nobs1, pval);
  run_higgssignals_stxs_(&csqmu2, &csqmh2, &csqtot2, &nobs2, pval);
  run_higgssignals_lhc_run1_combination_(&csqmu3, &csqmh3, &csqtot3, &nobs3, pval);

//   cout << csqmu1 << " " << csqmh1 << " " << csqtot1 << endl;
//   cout << csqmu2 << " " << csqmh2 << " " << csqtot2 << endl;

  *csqmu = csqmu1 + csqmu2 + csqmu3;
  *csqmh = csqmh1 + csqmh2 + csqmh3;
  *csqtot = *csqmu + *csqmh;
  *nobs = nobs1 + nobs2 + nobs3;

//   cout << *csqmu << " " << *csqmh << " " << *csqtot << endl;
}


void HS_set_pdf(int pdf) {

  setup_pdf_(&pdf);

}

void HS_set_assignment_range(double range) {
  setup_assignmentrange_(&range);
}

void HS_setup_assignment_range_massobservables(double range) {
  setup_assignmentrange_massobservables_(&range);
}

void HS_set_mass_uncertainties(double dMh[]) {
	higgssignals_neutral_input_massuncertainty_(dMh);
}

void HS_get_Rvalues(int i, int collider, double &R_H_WW, double &R_H_ZZ, double &R_H_gaga, double &R_H_tautau, double &R_H_bb, double &R_VH_bb) {
  get_rvalues_(&i, &collider, &R_H_WW, &R_H_ZZ, &R_H_gaga, &R_H_tautau, &R_H_bb, &R_VH_bb);

}

void HS_set_output_level(int level) {

  setup_output_level_(&level);

}

#endif

