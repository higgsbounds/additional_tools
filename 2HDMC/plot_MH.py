import matplotlib.pyplot as plt
import pylab as P
import numpy as np

P.rc("text", usetex=True)
font = {"size": 16}
P.rc("font", **font)
P.rc("grid", linewidth=1, color="#666666")

inputfile = "MHscan.dat"

data = np.loadtxt(inputfile)
dataT = list(zip(*data))

chi2min = min(dataT[22])
print(
    "Minimal chi^2 in scan data is ",
    chi2min,
    " at mH = ",
    dataT[1][dataT[22].index(chi2min)],
)

# Determine 1-dimensional 2sigma confidence region.
Deltachi2_2s = [np.abs(d - chi2min - 4.0) for d in dataT[22]]

chi2_2s = min(Deltachi2_2s)

index_2s = Deltachi2_2s.index(chi2_2s)
mH_2s = dataT[1][index_2s]

print("2sigma boundary at scan point ", index_2s, "with H mass: ", dataT[1][index_2s])

fig, ax = plt.subplots()
# -------- BR(h->HH) plot
outputfile = "MHscan_" + str(dataT[6][0]) + "_BR_h-HH.pdf"


labeltext = r"$\lambda_L = " + str(dataT[6][0]) + "$"
ax.plot(dataT[1], dataT[7], color="b", linestyle="-", linewidth=2, label=labeltext)
ax.set_xlim([min(dataT[1]), max(dataT[1])])
ax.set_ylim([0.0, 0.2])

ax.fill_between(dataT[1], 0.0, 1.0, where=dataT[1] <= mH_2s, alpha=0.25, color="orange")

plt.grid()

leg = plt.legend(fancybox=True, fontsize=14, loc=3)
leg.get_frame().set_alpha(0.5)

plt.xlabel(r"$m_{H}~[\mathrm{GeV}]$")
plt.ylabel(r"$\mathrm{BR}(h\to HH)$")

plt.savefig(outputfile)

# -------- chi^2 plot
outputfile = "MHscan_" + str(dataT[6][0]) + "_chi2.pdf"


fig, ax = plt.subplots()
ax.plot(dataT[1], dataT[22], color="b", linestyle="-", linewidth=2, label=labeltext)
ax.set_ylim([70.0, 100.0])

ax.fill_between(
    dataT[1], 0.0, 100.0, where=dataT[1] <= mH_2s, alpha=0.25, color="orange"
)


leg = plt.legend(fancybox=True, fontsize=14, loc=3)
leg.get_frame().set_alpha(0.5)

plt.xlabel(r"$m_{H}~[\mathrm{GeV}]$")
plt.ylabel(r"$\chi^2$")
plt.grid()

plt.savefig(outputfile)

# -------- HB plot
outputfile = "MHscan_" + str(dataT[6][0]) + "_HB.pdf"


fig, ax = plt.subplots()
ax.plot(dataT[1], dataT[12], color="b", linestyle="-", linewidth=2, label="obs (1st)")
ax.plot(dataT[1], dataT[16], color="r", linestyle="-", linewidth=2, label="obs (2nd)")
ax.plot(dataT[1], dataT[20], color="g", linestyle="-", linewidth=2, label="obs (3rd)")
ax.plot(dataT[1], dataT[13], color="b", linestyle="--", linewidth=2, label="exp (1st)")
ax.plot(dataT[1], dataT[17], color="r", linestyle="--", linewidth=2, label="exp (2nd)")
ax.plot(dataT[1], dataT[21], color="g", linestyle="--", linewidth=2, label="exp (3rd)")

ax.set_ylim([0.0, 4.0])

ax.fill_between(
    dataT[1], 0.0, 100.0, where=dataT[1] <= mH_2s, alpha=0.25, color="orange"
)


leg = plt.legend(fancybox=True, fontsize=14, loc=3)
leg.get_frame().set_alpha(0.5)

plt.xlabel(r"$m_{H}~[\mathrm{GeV}]$")
plt.ylabel(r"$\sigma/\sigma_\mathrm{limit}$")
plt.grid()

plt.savefig(outputfile)
